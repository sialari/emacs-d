;; cheatsheet.el configuration
(cheatsheet-add :group 'Org-mode
                :key "C-c a"
                :description "org-agenda")

(cheatsheet-add :group 'Org-mode
                :key "C-c c"
                :description "org-capture")

(cheatsheet-add :group 'Org-mode
                :key "C-x n n"
                :description "org narrow to region")

(cheatsheet-add :group 'Org-mode
                :key "C-x n w"
                :description "widen")
